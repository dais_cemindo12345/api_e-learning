<?php

class Elearning_model extends CI_Model 
{
	#ELEARNING
	
	# INSERT
	## INSETR CLASS
	 public function elearningInsertClass($data)
    {
        $this->elearning->insert('class', $data);
        return $this->elearning->affected_rows();
    }
	
	## INSERT ATTACHMENT
	 public function elearningInsertAttachment($data)
    {
        $this->elearning->insert('classAttachment', $data);
        return $this->elearning->affected_rows();
    }
	
		## INSERT ANSWER
	 public function elearningInsertAnswer($data)
    {
		$this->elearning->insert_batch('answer', $data);
        return $this->elearning->affected_rows();
    }
	
		## INSERT ANSWER
	 public function elearningInsertQuestion($data)
    {
		$this->elearning->insert('question', $data);
        return $this->elearning->affected_rows();
    }
	
		## INSERT ASSIGNMENT
	 public function elearningInsertAssignment($data)
    {
		$this->elearning->insert('assignment', $data);
        return $this->elearning->affected_rows();
    }
		## INSERT USER ANSWER
	 public function elearningInsertUserAnswer($data)
    {
		$this->elearning->insert_batch('userAnswer', $data);
        return $this->elearning->affected_rows();
    }
	
	# UPDATE 
		## CLASS
	public function elearningUpdateClass($data)
    {
        $this->elearning->set('imageStatus', $data['imageStatus']);
		$this->elearning->where('pictureName', $data['pictureName']);
        $this->elearning->update('class');
        return $this->elearning->affected_rows();
    }
	
		## DATA CLASS
	public function elearningUpdateDataClass($id, $data)
    {
        $this->elearning->where('id', $id);
        $this->elearning->update('class', $data);
        return $this->elearning->affected_rows();
    }
}