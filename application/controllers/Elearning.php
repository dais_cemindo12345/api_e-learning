<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Elearning extends RestController
{
    #CONSTRUCT
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Elearning_model', 'ElearningModel');
        $this->elearning = $this->load->database('elearning', TRUE);

        // timezone
        date_default_timezone_set('Asia/Jakarta');
    }

    # GET
    ## GET CLASS
    public function eLearning_get_class_by_finalCode_post()
    {
        $finalcode = $this->post('finalCode');

        $data = $this->elearning->get_where('class', ['finalCode' => $finalcode])->row_array();

        if ($data) {
            // Set the response and exit
            $output = array(
                'status' => TRUE,
                'result' => $data
            );
        } else {
            // Set the response and exit
            $output = array(
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            );
        }
        exit(json_encode($output));
    }

    ## GET CLASS
    public function eLearning_get_class_post()
    {
        $query = "
                SELECT C.id, C.pictureName, C.dateCreated, C.title, C.statusId, M.statusName, C.imageStatus
                FROM class C LEFT JOIN masterStatus M
                ON M.id = C.statusId
                ORDER BY C.dateCreated DESC
            ";
        $data = $this->elearning->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $output = array(
                'status' => TRUE,
                'result' => $data
            );
        } else {
            // Set the response and exit
            $output = array(
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            );
        }
        exit(json_encode($output));
    }

    ## GET Module
    public function eLearning_get_module_post()
    {
        $query = "
            SELECT DISTINCT class.moduleId, class.moduleName
            FROM class
            ORDER BY class.moduleName ASC
        ";
        $data = $this->elearning->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $output = array(
                'status' => TRUE,
                'result' => $data
            );
        } else {
            // Set the response and exit
            $output = array(
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            );
        }
        exit(json_encode($output));
    }

    ## GET Business Unit
    public function eLearning_get_business_unit_post()
    {
        $query = "
            SELECT *
            FROM businessUnit
            ORDER BY businessUnit.businessUnitName ASC
        ";
        $data = $this->elearning->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $output = array(
                'status' => TRUE,
                'result' => $data
            );
        } else {
            // Set the response and exit
            $output = array(
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            );
        }
        exit(json_encode($output));
    }

    ## GET Competence
    public function eLearning_get_competence_post()
    {
        $query = "
            SELECT *
            FROM competence
            ORDER BY competence.competenceName ASC
        ";
        $data = $this->elearning->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $output = array(
                'status' => TRUE,
                'result' => $data
            );
        } else {
            // Set the response and exit
            $output = array(
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            );
        }
        exit(json_encode($output));
    }

    ## GET CLASS BY ID
    public function eLearning_get_class_by_id_post()
    {
        $id = $this->post('id');
        $query = "
			SELECT *
			FROM class C LEFT JOIN masterStatus M
			ON M.id = C.statusId
			WHERE C.id = " . $id . "
		";
        $data = $this->elearning->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    ## GET CLASS BY ID
    public function eLearning_get_all_class_by_nik_concateClassCode_post()
    {
        $nik = $this->post('employeeNIK');
        $concateClassCode = $this->post('concateClassCode');
        $query = "
        SELECT A.*, C.title, C.pictureName, C.trainingObjectives, C.statusId, C.dateCreated, C.dateModified  FROM assignment A
        INNER JOIN class C
        ON A.classId = C.id
        WHERE A.employeeNIK = '$nik'
        AND C.finalCode LIKE '$concateClassCode%'
        AND C.statusId = 2
		";

        $data = $this->elearning->query($query)->num_rows();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    ## GET CLASS BY ID
    public function eLearning_get_class_by_id_row_post()
    {
        $id = $this->post('id');
        $query = "
			SELECT *
			FROM class C LEFT JOIN masterStatus M
			ON M.id = C.statusId
			WHERE C.id = " . $id . "
		";
        $data = $this->elearning->query($query)->row_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    ## GET ATTACHMENT BY ID
    public function eLearning_get_attachment_by_id_post()
    {
        $id = $this->post('id');
        $query = "
			SELECT * FROM classAttachment
			WHERE classId = " . $id . "
		";
        $data = $this->elearning->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    ## GET QUESTION BY ID CLASS
    public function eLearning_get_question_by_id_class_post()
    {
        $id = $this->post('id');
        $query = "
			SELECT * FROM question
			WHERE idClass = " . $id . "
			ORDER BY id DESC
		";

        $data = $this->elearning->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    ## GET QUESTION BY ID CLASS
    public function eLearning_get_question_by_id_class_asc_post()
    {
        $id = $this->post('id');
        $query = "
			SELECT * FROM question
			WHERE idClass = " . $id . "
			ORDER BY id ASC
		";

        $data = $this->elearning->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }


    ## GET QUESTION OPTION BY QUESTION CODE
    public function eLearning_get_option_question_by_question_code_post()
    {
        $id = $this->post('id');

        $query = "
			SELECT * FROM answerOption
			WHERE questionCode = '$id'
		";

        $data = $this->elearning->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    ## GET QUESTION OPTION BY QUESTION CODE
    public function eLearning_get_call_report_post()
    {
        $post = $this->post('params');
        $status_id = $post['status_id'];
        $initial_of_competence = $post['initial_of_competence'];
        $business_unit_id = $post['business_unit_id'];
        $module_id = $post['module_id'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];

        $query = "
        CALL report_class_row('$status_id', '$initial_of_competence', '$business_unit_id', '$module_id', '$start_date', '$end_date')
		";

        $data = $this->elearning->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    ## HCIS GET DATA KARYAWN
    public function hcis_get_data_karyawan_post()
    {

        $this->hcis = $this->load->database('hcis', TRUE);

        $search = $this->post('id');

        $query = "
			SELECT 
			karyawan.id,
			karyawan.NIK,
			karyawan.Nama_Lengkap,
			karyawan.Email,
			karyawan.Email_Pribadi,
			karyawan.Mulai_Bekerja,
			karyawan.Nama_Jabatan,
			daftardepartemen.Nama_departemen,
			daftardepartemenSub.Nama_Sub_Departemen,
			daftarperusahaan.Nama_Lkp_Perusahaan
			FROM karyawan
			LEFT JOIN hccorp.user
			ON hccorp.user.idkar = hccorp.karyawan.id
			LEFT JOIN hccorp.daftardepartemen
			ON hccorp.daftardepartemen.kode_departemen = hccorp.karyawan.Kode_Departemen
			LEFT JOIN  hccorp.daftardepartemensub
			ON hccorp.daftardepartemensub.Kode_Sub_Departemen=hccorp.karyawan.Kode_Sub_Departemen
			LEFT JOIN hccorp.daftarperusahaan
			ON hccorp.daftarperusahaan.Kode_Perusahaan=hccorp.karyawan.Kode_Perusahaan
			WHERE
			hccorp.user.active='Y'
			AND karyawan.NIK LIKE '%$search%' OR karyawan.Nama_Lengkap LIKE '%$search%'
		";

        $data = $this->hcis->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => "Data \"{$search}\" tidak ditemukan"
            ], 404);
        }
    }

    ## HCIS GET DATA KARYAWN
    public function hcis_get_data_karyawan_by_nik_post()
    {

        $this->hcis = $this->load->database('hcis', TRUE);

        $nik = $this->post('nik');

        $query = "
			SELECT 
			karyawan.id,
			karyawan.NIK,
			karyawan.Nama_Lengkap,
			karyawan.Email,
			karyawan.Email_Pribadi,
			karyawan.Mulai_Bekerja,
			karyawan.Nama_Jabatan,
			daftardepartemen.Nama_departemen,
			daftardepartemenSub.Nama_Sub_Departemen,
			daftarperusahaan.Nama_Lkp_Perusahaan
			FROM karyawan
			LEFT JOIN hccorp.user
			ON hccorp.user.idkar = hccorp.karyawan.id
			LEFT JOIN hccorp.daftardepartemen
			ON hccorp.daftardepartemen.kode_departemen = hccorp.karyawan.Kode_Departemen
			LEFT JOIN  hccorp.daftardepartemensub
			ON hccorp.daftardepartemensub.Kode_Sub_Departemen=hccorp.karyawan.Kode_Sub_Departemen
			LEFT JOIN hccorp.daftarperusahaan
			ON hccorp.daftarperusahaan.Kode_Perusahaan=hccorp.karyawan.Kode_Perusahaan
			WHERE
			hccorp.user.active='Y'
			AND karyawan.NIK = '$nik'
		";

        $data = $this->hcis->query($query)->row_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => "Data \"{$nik}\" tidak ditemukan"
            ], 404);
        }
    }

    ## GET KARYAWN BY ID
    public function eLearning_get_assignment_by_classid_post()
    {
        $id = $this->post('id');

        $data = $this->elearning->get_where('assignment', ['classId' => $id])->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    ## GET DATA ASSIGNMENT
    public function eLearning_get_assignment_post()
    {
        $data = $this->elearning->get('assignment')->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    ## GET DATA ASSIGNMENT
    public function eLearning_get_assignment_by_employeeid_and_classid_post()
    {
        $employeeId = $this->post('employeeId');
        $classId = $this->post('classId');

        $this->elearning->where('employeeId', $employeeId);
        $this->elearning->where('classId', $classId);
        $data = $this->elearning->get('assignment')->row_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    ## GET DATA ASSIGNMENT
    public function eLearning_get_assignment_by_nik_post()
    {
        $nik = $this->post('nik');

        $query = "SELECT A.*, C.title, C.subClassName, C.pictureName, C.trainingObjectives, C.dateCreated, C.dateModified, C.activeClass, C.moduleName, expiredClass, M.statusName  FROM assignment A
				INNER JOIN class C
				ON A.classId = C.id
				INNER JOIN masterStatus M
				ON A.assignmentStatus = M.id
				WHERE A.employeeNIK = '$nik'
				AND C.statusId = 2
				GROUP BY A.concateClassCode
				ORDER BY C.statusId DESC
				";

        $data = $this->elearning->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    ## GET DATA ASSIGNMENT
    public function eLearning_get_assignment_nik_post()
    {
        $nik = $this->post('nik');

        $query = "SELECT A.*, C.title, C.subClassName, C.pictureName, C.trainingObjectives, C.dateCreated, C.dateModified, C.activeClass, C.moduleName, expiredClass, M.statusName  FROM assignment A
				INNER JOIN class C
				ON A.classId = C.id
				INNER JOIN masterStatus M
				ON A.assignmentStatus = M.id
				WHERE A.employeeNIK = '$nik'
				AND C.statusId = 2
				ORDER BY C.statusId DESC
				";

        $data = $this->elearning->query($query)->result_array();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    ## GET DATA ASSIGNMENT
    public function eLearning_get_assignment_status_post()
    {
        $nik = $this->post('nik');

        //$data = $this->elearning->query($query)->result_array();

        $this->elearning->trans_start();

        // get count all class
        $query_all_sub_class = "SELECT A.*, C.title, C.pictureName, C.trainingObjectives, C.statusId, C.dateCreated, C.dateModified  FROM assignment A
				INNER JOIN class C
				ON A.classId = C.id
				WHERE A.employeeNIK = '$nik'
				AND C.statusId = 2
				";
        $count_all_sub_class = $this->elearning->query($query_all_sub_class)->num_rows();

        // get count all class
        $query_all_class = "SELECT A.*, C.title, C.pictureName, C.trainingObjectives, C.statusId, C.dateCreated, C.dateModified  FROM assignment A
				INNER JOIN class C
				ON A.classId = C.id
				WHERE A.employeeNIK = '$nik'
				AND C.statusId = 2
				GROUP BY A.concateClassCode
				";
        $count_all_class = $this->elearning->query($query_all_class)->num_rows();

        // get count class by assignmentStatus = 4
        $query_active_class = "SELECT A.*, C.title, C.pictureName, C.trainingObjectives, C.statusId, C.dateCreated, C.dateModified  FROM assignment A
				INNER JOIN class C
				ON A.classId = C.id
				WHERE A.employeeNIK = '$nik'
				AND C.statusId = 2
				AND A.assignmentStatus = 4
				";
        $count_active_class = $this->elearning->query($query_active_class)->num_rows();

        // get count class by assignmentStatus = 4
        $query_done_class = "SELECT A.*, C.title, C.pictureName, C.trainingObjectives, C.statusId, C.dateCreated, C.dateModified  FROM assignment A
				INNER JOIN class C
				ON A.classId = C.id
				WHERE A.employeeNIK = '$nik'
				AND C.statusId = 2
				AND A.assignmentStatus = 5
				";
        $count_done_class = $this->elearning->query($query_done_class)->num_rows();

        $data = [
            'subClassTotal' => $count_all_sub_class,
            'classTotal' => $count_all_class,
            'classActive' => $count_active_class,
            'classDone' => $count_done_class,
        ];

        $this->elearning->trans_complete();

        if ($this->elearning->trans_status() === FALSE) {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        } else {

            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $data
            ], 200);
        }
    }

    ## GET DATA ASSIGNMENT
    public function eLearning_get_score_by_class_post()
    {
        $nik = $this->post('employeeNIK');
        $concateClassCode = $this->post('concateClassCode');

        //$data = $this->elearning->query($query)->result_array();

        $this->elearning->trans_start();

        // get SUM point
        $query_sum_point = "
                    SELECT SUM(P.point) AS point
                    FROM classPoint P
                    WHERE P.employeeNIK = '$nik'
                    AND P.concateClassCode = '$concateClassCode'
				";
        $point = $this->elearning->query($query_sum_point)->row_array();
        // get count class assignment
        $query_count_class_assignment = "
                    SELECT COUNT(*) AS countClass 
                    FROM assignment A
                    WHERE A.employeeNIK = '$nik'
                    AND A.concateClassCode = '$concateClassCode'
				";
        $countClass = $this->elearning->query($query_count_class_assignment)->row_array();

        $score = $point['point'] / $countClass['countClass'];

        $this->elearning->trans_complete();

        if ($this->elearning->trans_status() === FALSE) {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            ], 404);
        } else {

            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'result' => $score
            ], 200);
        }
    }

    ## CREATE SCORE
    public function eLearning_create_score_post()
    {
        $data = $this->post();
        $classId = $data['classId'];
        $employeeNIK = $data['employeeNIK'];
        $this->elearning->trans_begin();

        //query
        // get user answer
        $query = "
			SELECT A.*, Q.questionType, Q.questionPoint, B.correctAnswer
			FROM userAnswer A
			INNER JOIN question Q
			ON Q.id = A.questionId
			INNER JOIN answerOption B
			ON B.id = A.answerOptionId
			WHERE Q.questionType = 'mc'
			AND A.classId = '$classId'
			AND A.employeeNIK = '$employeeNIK'
            ";

        $user_answer = $this->elearning->query($query)->result_array();


        $score = 0;
        $get_point = 0;

        for ($i = 0; $i < count($user_answer); $i++) {
            $score += $user_answer[$i]['questionPoint'];

            if ($user_answer[$i]['correctAnswer'] == 1) {
                $get_point += $user_answer[$i]['questionPoint'];
            }
        }

        $score = (int)$score;
        $get_point = (int)$get_point;

        $score_point = round((($get_point / $score) * 100), 1);

        if ($score_point < 80) {
            /* 
				Ubah assignmentStatus menjadi 4
			*/

            $this->elearning->set('assignmentStatus', 4);
            $this->elearning->where('classId', $data['classId']);
            $this->elearning->where('employeeNIK', $data['employeeNIK']);
            $this->elearning->update('assignment');

            $this->elearning->where('classId', $data['classId']);
            $this->elearning->where('employeeNIK', $data['employeeNIK']);
            $this->elearning->delete('userAnswer');

            var_dump($this->elearning->trans_status());
            die;

            if ($this->elearning->trans_status() === FALSE) {
                $this->elearning->trans_rollback();

                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'result' => 'Data gagal di input'
                ], 404);
            } else {
                $this->elearning->trans_commit();

                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'score' => $score_point
                ], 200);
            }

            exit;
        }

        $params = [
            'classId'           => $data['classId'],
            'employeeNIK'       => $data['employeeNIK'],
            'concateClassCode'  => $data['concateClassCode'],
            'finalCode'         => $data['finalCode'],
            'classTitle'        => $data['classTitle'],
            'processingTime'    => $data['processingTime'],
            'point'             => $score_point,
            'submitDate'        => date('Y-m-d H:i:s')
        ];

        $this->elearning->insert('classPoint', $params);


        if ($this->elearning->trans_status() === FALSE) {
            $this->elearning->trans_rollback();

            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'result' => 'Data gagal di input'
            ], 404);
        } else {
            $this->elearning->trans_commit();

            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'score' => $score_point
            ], 200);
        }
    }

    ## GET CLASS POINT
    public function eLearning_dashboard_score_post()
    {

        $post = $this->post();
        $employeeNIK = $this->post('employeeNIK');
        $data = $this->elearning->get_where('classPoint', ['employeeNIK' => $employeeNIK])->result_array();

        if ($data) {
            // Set the response and exit
            $output = array(
                'status' => TRUE,
                'result' => $data
                # 'result' => $this->post('employeeNIK')
            );
        } else {
            // Set the response and exit
            $output = array(
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            );
        }
        exit(json_encode($output));
    }

    public function eLearning_class_point_post()
    {

        $post = $this->post('finalCode');
        $data = $this->elearning->get_where('classPoint', ['finalCode' => $post])->row_array();

        if ($data) {
            // Set the response and exit
            $output = array(
                'status' => TRUE,
                'result' => $data
                # 'result' => $this->post('employeeNIK')
            );
        } else {
            // Set the response and exit
            $output = array(
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            );
        }
        exit(json_encode($output));
    }

    # CHECK FINAL CODE
    public function eLearning_check_finalcode_post()
    {
        $finalCode = $this->post('id');

        $data = $this->elearning->get_where('class', ['finalCode' => $finalCode])->num_rows();

        if ($data) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
            ], 404);
        }
    }

    # CHECK CODE COPETENCE
    public function eLearning_get_codeCopetence_post()
    {
        $data = $this->elearning->get('competence')->result_array();

        if ($data) {
            // Set the response and exit
            $output = array(
                'status' => TRUE,
                'result' => $data
                # 'result' => $this->post('employeeNIK')
            );
        } else {
            // Set the response and exit
            $output = array(
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            );
        }

        exit(json_encode($output));
    }

    # CHECK CODE BUSINESS UNIT
    public function eLearning_get_businessUnit_post()
    {
        $data = $this->elearning->get('businessUnit')->result_array();

        if ($data) {
            // Set the response and exit
            $output = array(
                'status' => TRUE,
                'result' => $data
                # 'result' => $this->post('employeeNIK')
            );
        } else {
            // Set the response and exit
            $output = array(
                'status' => FALSE,
                'result' => 'Data tidak ditemukan'
            );
        }

        exit(json_encode($output));
    }

    # INSERT 
    ## INSERT CLASS
    public function eLearning_insert_class_post()
    {
        $class = $this->post('class');

        $this->elearning->trans_begin();

        $this->elearning->insert('class', $class);

        if ($this->elearning->trans_status() === FALSE) {
            $this->elearning->trans_rollback();

            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal ditambahkan'
            ], 404);
        } else {
            $this->elearning->trans_commit();

            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil ditambahkan'
            ], 200);
        }
    }

    ## INSERT ATTACHMENT
    public function eLearning_insert_attachment_post()
    {
        $data = [
            'classId' => $this->post('classId'),
            'title' => $this->post('title'),
            'attachmentName' => $this->post('attachmentName'),
            'attachmentType' => $this->post('attachmentType'),
        ];

        if ($this->ElearningModel->elearningInsertAttachment($data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil ditambahkan'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal ditambahkan'
            ], 404);
        }
    }

    ## INSERT QUESTION
    public function eLearning_insert_question_p_post()
    {
        $data = $this->post('dataQuestion');

        if ($this->ElearningModel->elearningInsertQuestion($data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil ditambahkan'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal ditambahkan'
            ], 404);
        }
    }

    ## INSERT QUESTION AND OPTION MC
    public function eLearning_insert_question_mc_post()
    {
        $question = $this->post('dataQuestion');
        $answer = $this->post('dataAnswer');

        $this->elearning->trans_begin();

        $this->elearning->insert('question', $question);
        $this->elearning->insert_batch('answerOption', $answer);

        if ($this->elearning->trans_status() === FALSE) {
            $this->elearning->trans_rollback();

            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal ditambahkan'
            ], 404);
        } else {
            $this->elearning->trans_commit();

            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil ditambahkan'
            ], 200);
        }
    }

    ## INSERT ASSIGNMENT
    public function eLearning_insert_assignment_post()
    {
        $data = [
            'employeeId'        => $this->post('employeeId'),
            'employeeNIK'        => $this->post('employeeNIK'),
            'employeeName'        => $this->post('employeeName'),
            'classId'            => $this->post('classId'),
            'concateClassCode'            => $this->post('classCode'),
            'finalCode'            => $this->post('finalCode'),
            'assignmentStatus'  => 4
        ];

        $this->elearning->where('employeeNIK =', $this->post('employeeNIK'));
        $this->elearning->where('classId =', $this->post('classId'));
        $result = $this->elearning->get('assignment')->row_array();

        if (!$result) {
            if ($this->ElearningModel->elearningInsertAssignment($data) > 0) {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'Data berhasil ditambahkan'
                ], 200);
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'Data gagal ditambahkan'
                ], 404);
            }
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data sudah ada'
            ], 404);
        }
    }

    ## INSERT MC USER ANSWER
    public function eLearning_insert_user_answer_post()
    {
        $data = $this->post('params');
        $p = $this->post('p');
        $employeeNIK = $this->post('employeeNIK');
        $classId = $this->post('classId');
        $processingTime = $this->post('processingTime');

        $this->elearning->trans_begin();

        if (count($data) > 0) {
            $this->elearning->insert_batch('userAnswer', $data);
        }

        if (count($p) > 0) {
            $this->elearning->insert_batch('userAnswer', $p);
        }

        $query = "
			UPDATE assignment
			SET assignmentStatus = 5
			WHERE employeeNIK = " . $employeeNIK . "
			AND classId = " . $classId . ";
		";

        $this->elearning->query($query);

        // Get class title
        $get_title = $this->elearning->get_where('class', ['id' => $classId])->row_array();

        if ($this->elearning->trans_status() === FALSE) {
            $this->elearning->trans_rollback();

            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Kelas ' . $get_title['title'] . ' gagal di selesaikan'
            ], 404);
        } else {
            $this->elearning->trans_commit();

            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Kelas ' . $get_title['title'] . ' telah di selesaikan'
            ], 200);
        }
    }

    # UPDATE
    ## UPDATE CLASS
    public function eLearning_update_class_post()
    {
        $data = [
            'title' => $this->post('title'),
            'pictureName' => $this->post('pictureName'),
            'imageStatus' => $this->post('imageStatus')
        ];

        if ($this->ElearningModel->elearningUpdateClass($data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil diubah'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal diubah'
            ], 404);
        }
    }

    ## UPDATE DATA CLASS
    public function eLearning_update_data_class_post()
    {
        $data = [
            'title' => $this->post('title'),
            'activeClass' => $this->post('activeClass'),
            'expiredClass' => $this->post('expiredClass'),
            'pictureName' => $this->post('pictureName'),
            'trainingObjectives' => $this->post('trainingObjectives'),
            'modifiedBy' => $this->post('modifiedBy'),
            'dateModified' => $this->post('dateModified')
        ];

        $id = $this->post('id');

        if ($this->ElearningModel->elearningUpdateDataClass($id, $data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil diubah'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal diubah'
            ], 404);
        }
    }

    ## UPDATE DATA CLASS
    public function eLearning_update_data_class_publish_post()
    {
        $data = [
            'statusId' => $this->post('statusId')
        ];

        $id = $this->post('id');

        if ($this->ElearningModel->elearningUpdateDataClass($id, $data) > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil diubah'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal diubah'
            ], 404);
        }
    }

    # DELETE
    ## DELETE ATTACHMENT
    public function eLearning_delete_attachment_post()
    {

        $id = $this->post('id');

        $this->elearning->where('id', $id);
        $result = $this->elearning->delete('classAttachment');

        if ($result > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil dihapus'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal dihapus'
            ], 404);
        }
    }

    ## DELETE CLASS
    public function eLearning_delete_class_post()
    {

        $id = $this->post('id');

        $this->elearning->trans_begin();

        // cek data lampiran
        $getAttachment = $this->elearning->get_where('classAttachment', ['classId' => $id])->result_array();

        if (!empty($getAttachment)) {
            $this->elearning->where('classId', $id);
            $this->elearning->delete('classAttachment');
        }

        // cek data assignment
        $getAssignment = $this->elearning->get_where('assignment', ['classId' => $id])->result_array();

        if (!empty($getAssignment)) {
            $this->elearning->where('classId', $id);
            $this->elearning->delete('assignment');
        }

        $getClass = $this->elearning->get_where('class', ['id' => $id])->row_array();

        $this->elearning->where('id', $id);
        $this->elearning->delete('class');


        if ($this->elearning->trans_status() === FALSE) {
            $this->elearning->trans_rollback();

            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal dihapus'
            ], 404);
        } else {
            $this->elearning->trans_commit();

            // Set the response and exit
            $this->response([
                'status'      => TRUE,
                'message'     => 'Data berhasil dihapus',
                'data'        => $getAttachment,
                'pictureName' => $getClass['pictureName']
            ], 200);
        }
    }

    ## DELETE ASSIGNMENT
    public function eLearning_delete_assignment_post()
    {

        $employeeNIK = $this->post('employeeNIK');
        $classId = $this->post('classId');

        $this->elearning->where('employeeNIK = ', $employeeNIK);
        $this->elearning->where('classId = ', $classId);
        $result = $this->elearning->delete('assignment');

        if ($result > 0) {
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil dihapus'
            ], 200);
        } else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal dihapus'
            ], 404);
        }
    }

    ## DELETE QUESTION
    public function eLearning_delete_question_post()
    {

        $questionCode = $this->post('questionCode');

        $this->db->trans_begin();


        $tables = ['question', 'answerOption'];
        $this->elearning->where('questionCode = ', $questionCode);
        $this->elearning->delete($tables);

        if ($this->elearning->trans_status() === FALSE) {
            $this->elearning->trans_rollback();
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'Data gagal dihapus'
            ], 404);
        } else {
            $this->elearning->trans_commit();
            // Set the response and exit
            $this->response([
                'status' => TRUE,
                'message' => 'Data berhasil dihapus'
            ], 200);
        }
    }
}
